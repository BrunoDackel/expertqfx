package sample;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class xmlManager {

    private static final URL url = xmlManager.class.getResource("QA.xml"); //TODO messes with out... dunno

    /**
     * Reads the xml file QA.xml and extract all question String in the question nodes.
     * @return ArrayList with Strings of all question in QA.xml
     */
    public static ArrayList<String> readQuestions() {
        ArrayList<String> returnArrayList = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            File f = new File(url.getPath());
            Document doc = builder.parse(f);

            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath path = xPathFactory.newXPath();

            int numberOfEntries = Integer.parseInt(path.evaluate("count(/QAS/QA)",doc));
            for (int i = 1; i <= numberOfEntries ; i++) {
                String question = path.evaluate("/QAS/QA["+i+"]/question", doc);
                returnArrayList.add(i - 1, question);
            }
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            e.printStackTrace();
        }
        return returnArrayList;
    }

    /**
     * Reads the xml file QA.xml and extract all answers String in the question nodes. This method will convert the strings into bool´s so make sure that the string in QA is either "true" or "false"
     * @return ArrayList with Booleans´s of all answers in QA.xml
     */
    public static ArrayList<Boolean> readAnswers() {
        ArrayList<Boolean> returnArrayList = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            File f = new File(url.getPath());
            Document doc = builder.parse(f);

            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath path = xPathFactory.newXPath();

            int numberOfEntries = Integer.parseInt(path.evaluate("count(/QAS/QA)", doc));
            for (int i = 1; i <= numberOfEntries; i++) {
                String question = path.evaluate("/QAS/QA[" + i + "]/answer", doc);
                returnArrayList.add(i - 1, question.equals("true"));
            }
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            e.printStackTrace();
        }
        return returnArrayList;
    }

    /**
     * Will create a new QA Element in QA.xml. The QA element will have two children, question and answer
     * @param question String that will be the question for the QA node
     * @param answer String (MUST be "true" or "false") that will be the answer for the QA node
     */
    public static void createNewQAElement(String question, String answer){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            File f = new File(url.getPath());
            Document doc = builder.parse(f);

            Element root = doc.getDocumentElement();

            Element newQA = doc.createElement("QA");

            Element newQuestion = doc.createElement("question");
            newQuestion.appendChild(doc.createTextNode(question));
            newQA.appendChild(newQuestion);

            Element newAnswer = doc.createElement("answer");
            newAnswer.appendChild(doc.createTextNode(answer));
            newQA.appendChild(newAnswer);

            root.appendChild(newQA);

            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult(url.getPath());

            transformer.transform(source,result);
        }catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes a QA node by index (because i suck at programming). Remember that the index of xml files start with 1.
     * @param index The index of the QA nodes that will be deleted
     */
    public static void deleteQAElement(int index){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            File f = new File(url.getPath());
            Document doc = builder.parse(f);

            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath path = xPathFactory.newXPath();

            XPathExpression expression = path.compile("/QAS/QA["+index+"]");

            Node node = (Node) expression.evaluate(doc, XPathConstants.NODE);
            node.getParentNode().removeChild(node);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult(url.getPath());

            DOMSource source = new DOMSource(doc);
            transformer.transform(source,result);
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException | TransformerException e) {
            e.printStackTrace();
        }
    }
}

package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("design.fxml"));
        primaryStage.setTitle("Expert Quiz!");
        primaryStage.setScene(new Scene(root, 400, 400));

        stage.setOnCloseRequest(e ->{
            e.consume();
            stage.close();
        });

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

package sample;

import java.util.ArrayList;


public class Game {

    private String[] questions;
    private boolean[] answers;
    private String[] playerNames;
    private int numberOfPlayers;
    private int[] score;
    private ArrayList<Integer> usedQuestions;
    private int numberOfQuestionsPerPlayer;
    private boolean reusingQuestions;
    private int currentQuestionIndex;
    private int questionAsked;


    public Game(String[] playerNames, int numberOfQuestionsPerPlayer) {
        this.questions = initQuestions();
        this.answers = initAnswers();
        this.playerNames = playerNames;
        numberOfPlayers = this.playerNames.length;
        score = new int[this.playerNames.length];
        usedQuestions = new ArrayList<>();
        this.numberOfQuestionsPerPlayer = numberOfQuestionsPerPlayer;
        reusingQuestions = willReuseQuestions();
        currentQuestionIndex = -1;
        questionAsked = 0;
    }

    /**
     * Will return true if the game is finished. We know if the game is finished when the number of question asked is
     * greater or equal to number of players multiplied by number of questions per player
     * @return true if the game has ended, false otherwise
     */
    public boolean isGameFinished() {
        boolean returnBoolean = false;
        if (questionAsked >= numberOfPlayers * numberOfQuestionsPerPlayer) {
            returnBoolean = true;
        }
        return returnBoolean;
    }

    /**
     * Compares the parameter with the current question (indicated by the current question index) and returns a boolean when it is correct or incorrect.
     * What answer/question we compare is dependant on the currentQuestionIndex
     * Is in charge of keeping track of the score by calling {@link sample.Game#awardPoint() awardPoint()}.<B><P>
     * Will increment the questionAsked pointer, so don't call it multiple times.
     * @param answer True if the user answered yes
     * @return True if the answer was correct
     */
    public boolean isAnswerCorrect(boolean answer) {
        boolean returnBoolean = false;
        if (answer == answers[currentQuestionIndex]) {
            returnBoolean = true;
            awardPoint();
        }
        questionAsked++;
        return returnBoolean;
    }

    /**
     * Awards the points for the players. Note that the method is dependant on the currentQuestionIndex.
     */
    private void awardPoint(){
        int oldInt = score[(questionAsked % numberOfPlayers)];
        oldInt++;
        score[(questionAsked % numberOfPlayers)] = oldInt;
    }

    /**
     * Returns the next random question. If the amount of questions to be asked is greater than the available questions we will reuse questions.
     * @return String with the next question
     */
    public String getNextQuestions() {
        String returnString;
        int randomInt = randomInt(0, questions.length - 1);
        if (!reusingQuestions) { //If we reuse question we don't care
            returnString = questions[randomInt];
        } else { //If we don't reuse questions we will search for an unused question
            while (usedQuestions.contains(randomInt)) {
                randomInt = randomInt(0, questions.length - 1);
            }
            returnString = questions[randomInt];
            usedQuestions.add(randomInt);
        }
        currentQuestionIndex = randomInt;
        return returnString;
    }

    /**
     * Return the current players name. Note that this method need to be called before {@link sample.Game#isAnswerCorrect(boolean) isAnswerCorrect(boolean)}, since the name is dependant on the currentQuestionIndex
     * @return String with the players name
     */
    public String getPlayersTurnName(){
        String returnString;
        returnString = playerNames[(questionAsked % numberOfPlayers)];
        return returnString;
    }

    /**
     * Call at initialisation. Will load the object with the Question Array with the help of {@link xmlManager#readQuestions() xmlManager.readQuestions()}
     * @return String[] with questions
     */
    private String[] initQuestions() {
        ArrayList<String> tempArrayList = xmlManager.readQuestions();

        String[] returnArray = new String[tempArrayList.size()];
        for (int i = 0; i < tempArrayList.size(); i++) {
            returnArray[i] = tempArrayList.get(i);
        }
        return returnArray;
    }

    /**
     * Call at initialisation. Will load the object with the answer Array with the help of {@link xmlManager#readAnswers()} () xmlManager.readAnswers()}
     * @return boolean[] with answers
     */
    private boolean[] initAnswers() {
        ArrayList<Boolean> tempArrayList = xmlManager.readAnswers();

        boolean[] returnArray = new boolean[tempArrayList.size()];
        for (int i = 0; i < tempArrayList.size(); i++) {
            returnArray[i] = tempArrayList.get(i);

        }
        return returnArray;
    }

    /**
     * Call at initialisation. Will check if we reuse questions
     * @return True if we reuse Questions
     */
    private boolean willReuseQuestions() {
        boolean returnBoolean = true;
        if (questions.length < numberOfQuestionsPerPlayer * numberOfQuestionsPerPlayer) {
            returnBoolean = false;
        }
        return returnBoolean;
    }

    /**
     * Return a radon int
     * @param min the minimum value of the return int (inclusive)
     * @param max the maximum value of the return int (inclusive)
     * @return a random int
     */
    private static int randomInt(int min, int max) {
        int range = (max - min) + 1;
        return ((int) (Math.random() * range) + min);
    }

    /**
     * @return int[] of the current score
     */
    public int[] getScore() {
        return score;
    }

    /**
     * @return String[] of all players playing the game
     */
    public String[] getPlayerNames(){
        return playerNames;
    }

    /**
     * @return The number of different question loaded at initialisation
     */
    public int getNumberOfUniqueQuestions(){
        return questions.length;
    }
}

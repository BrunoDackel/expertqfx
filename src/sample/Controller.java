package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Controller {

    @FXML
    private ToggleGroup group;
    @FXML
    public Label mainDisplay;
    @FXML
    private Button yesButton;
    @FXML
    private Button noButton;

    private static int questionsPerPlayer = 3;
    private static String[] names = {"Player 1", "Player 2", "Player 3", "Player 4", "Player 5", "Player 6"};
    private static int numberOfPlayers = 2;

    private GameRunnable gameRunnable;
    public Lock continueLock = new ReentrantLock();
    public Condition continueCondition = continueLock.newCondition();

    /**
     * Starting method to initialize a new Game. When we start a new we make sure that we have some question to ask the player.
     * If this is not the case, we will {@link Controller#noQuestionErrorWindow() alert} the user and prompt him to input at least one question.
     * If we have at least one question we will activate the buttons, make them visible and call playGame()
     * @param event the event that starts this code block
     */
    @FXML
    public void handleNewGame(ActionEvent event) {
        String gameNames[] = Arrays.copyOfRange(names, 0, numberOfPlayers);
        Game game = new Game(gameNames, questionsPerPlayer);
        if (game.getNumberOfUniqueQuestions() == 0){
            noQuestionErrorWindow();
            return;
        }
        yesButton.setDisable(false);
        yesButton.setVisible(true);
        noButton.setDisable(false);
        noButton.setVisible(true);
        playGame(game);
    }

    /**
     *In this method we will start the game, by starting a runnable called GameRunnable. We will make sure that only one GameRunnable
     * is running at the time
     * @param game the Game object (logic) we want to play with
     */
    private void playGame(Game game) {
        //Kill previous runnable
        if (gameRunnable != null) {
            gameRunnable.setShutdown(true);
        }
        //make new runnable
        GameRunnable MyGameRunnable = new GameRunnable(this, game);
        gameRunnable = MyGameRunnable;
        Thread th = new Thread(MyGameRunnable);
        th.start();
    }

    /**
     * Disables the yes and no button to prevent unwanted user input
     */
    public void endGame() {
        yesButton.setDisable(true);
        noButton.setDisable(true);
    }

    /**
     * A simple alert window that will prompt the player to at least enter one question.
     * Will only be executed from {@link sample.Controller#handleNewGame(ActionEvent) handleNewGame()} if there are no question present.
     */
    private void noQuestionErrorWindow(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText("No question present");
        alert.setContentText("Please enter at least one question to the game!");
        alert.showAndWait();
    }


    /**
     * Sets the wanted player count, and makes sure that only one menuItem is toggled via toggle Group
     * @param event the event that starts this code block
     */
    @FXML
    public void handleSetPlayerCount(ActionEvent event) {
        RadioMenuItem selectedRadioButton = (RadioMenuItem) group.getSelectedToggle();
        String toogleGroupValue = selectedRadioButton.getText();
        switch (toogleGroupValue) {
            case "2 players":
                numberOfPlayers = 2;
                break;
            case "3 players":
                numberOfPlayers = 3;
                break;
            case "4 players":
                numberOfPlayers = 4;
                break;
            case "5 players":
                numberOfPlayers = 5;
                break;
            case "6 players":
                numberOfPlayers = 6;
                break;
        }
        System.out.println(numberOfPlayers);
    }

    /**
     * Handel's the name Change option. This method will make a dialog window with {@link sample.Controller#setNameDialogWindow(String) setNameDialogWindow}.
     * @param event the event that starts this code block
     */
    @FXML
    public void handleNameChange(ActionEvent event) {
        MenuItem menuItem = (MenuItem) event.getSource();
        String oldName = menuItem.getText();

        int index = Arrays.asList(names).indexOf(oldName);

        String newName = setNameDialogWindow(oldName).orElse(null);
        if (newName != null && !newName.equals("")) {
            names[index] = newName;
            menuItem.setText(newName);
        }
    }

    /**
     * Makes a Dialog window where the user is prompted to enter a new name.
     * @param name The old name of the user
     * @return A Optional String with the new name for the user
     */
    private Optional<String> setNameDialogWindow(String name) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Set new name");
        dialog.setHeaderText("Enter a new name for " + name);
        dialog.setContentText("Enter text");
        Optional<String> result;

        result = dialog.showAndWait();
        if (result.isPresent()) {
            result.get();
        }
        return result;
    }

    /**
     * Handel's the option the set the questions each player will get.
     * This method will create a dialog with {@link Controller#setNumberOfQuestionsDialogWindow() setNumberOfQuestionsDialogWindow}.
     * We will not allow negative input.
     * @param event the event that starts this code block
     */
    @FXML
    public void handleSetAmountQuestions(ActionEvent event) {
        boolean exitMenu = false;
        while (!exitMenu) {
            String result = setNumberOfQuestionsDialogWindow().orElse(null);
            if (result == null || result.equals("")) {
                exitMenu = true;
            } else if (isNonNegativeInteger(result)) {
                questionsPerPlayer = Integer.parseInt(result);
                exitMenu = true;
            }
        }

    }

    /**
     * A Dialog window that will prompt the user th input a number
     * @return A optional String with the ne amount of questions per player
     */
    private Optional<String> setNumberOfQuestionsDialogWindow() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Set number of Questions");
        dialog.setHeaderText("Enter how many questions each player will get");
        dialog.setContentText("Enter a number");
        Optional<String> result;

        result = dialog.showAndWait();

        return result;
    }

    /**
     * Helper method that cheks if a String is a non-negative integer. If it is not it will also create a alert window for the user
     * @param textInput The String we want to check
     * @return True if the String is an int
     */
    private boolean isNonNegativeInteger(String textInput) {
        int num;
        try {
            num = Integer.parseInt(textInput);
            if (num<0){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("Wrong input!");
                alert.setContentText("Please enter a number that is greater than 0!");
                alert.showAndWait();
                return false; // dirty way to code, please don't hate me Frank
            }
        } catch (NumberFormatException ne) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Wrong input!");
            alert.setContentText("Enter a number with witch decimals (0-9)");
            alert.showAndWait();
            return false;
        }
        return true;
    }

    /**
     * This method will prompt our await() GameRunnable to resume activity, after we supplied the GameRunnable with an answer.
     * Note that this method can generate a Deadlock if the user is allowed to press the yes or no button, while there is no GameRunnable active.
     * @param event the event that starts this code block
     */
    @FXML
    public void handleAnswer(ActionEvent event) {
        Button button = (Button) event.getSource();
        String pressedButton = button.getText();
        if (pressedButton.equals("Yes")) {
            gameRunnable.setAnswer(true);
        } else {
            gameRunnable.setAnswer(false);
        }
        while (continueLock.tryLock()) {
            try {
                continueCondition.signalAll();
            } finally {
                continueLock.unlock();
            }

        }
    }

    /**
     * Handel's the option to add a new question to the game.
     * First we will call {@link sample.Controller#enterQuestionDialogWindow() enterQuestionDialogWinodw} to ask the user for a new Question.
     * Afterwards we will call {@link sample.Controller#enterAnswerForNewQDialogWindow(String) enterAnswerForNewQDialogWindow} to ask the user for the answer to this question.
     * In the end we add this question and answers as a node to our xml QA.xml with {@link sample.xmlManager#createNewQAElement(String, String) createNewQAElement}.
     * Note that the answer node must be either "true" or "false"
     * @param event
     */
    @FXML
    public void handleAddQuestion(ActionEvent event) {
        String newQuestion = enterQuestionDialogWindow().orElse(null);
        if (newQuestion != null && !newQuestion.equals("")) {
            String newAnswer = enterAnswerForNewQDialogWindow(newQuestion);
            if (newAnswer != null && !newAnswer.equals("")) {
                xmlManager.createNewQAElement(newQuestion, newAnswer);
            }
        }
    }

    /**
     * A Dialog window that will ask the user for a new question.
     * @return A optional String with the new question
     */
    private Optional<String> enterQuestionDialogWindow() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Enter new Question");
        dialog.setHeaderText("Enter a new yes or no question");
        dialog.setContentText("Enter text (remember the ?)");
        Optional<String> result;

        result = dialog.showAndWait();
        return result;
    }

    /**
     * A Dialog window that will ask the user for an answer to a new question.
     * @param newQuestion the new question we want to add
     * @return The answer ("true" for yes and "false" for no) to the questions
     */
    private String enterAnswerForNewQDialogWindow(String newQuestion) {
        String returnString = null;
        List<String> choices = new ArrayList<>();
        choices.add("Yes");
        choices.add("No");

        ChoiceDialog<String> dialog = new ChoiceDialog<>("Choose answer", choices);
        dialog.setTitle("Enter new Question");
        dialog.setHeaderText("What is the answer for:\n " + newQuestion );
        dialog.setContentText("Choose the answer:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (result.get().equals("Yes")) {
                returnString = "true";
            } else if(result.get().equals("No")){
                returnString = "false";
            }
        }
        return returnString;
    }

    /**
     * Handel's the options to remove a question from the game permanently.
     * This method will call {@link Controller#removeQuestionDialogWindow() removeQuestionDialogWindow} to get the user Input.
     * @param event the event that starts this code block
     */
    @FXML
    public void handleRemoveQuestion(ActionEvent event) {
        removeQuestionDialogWindow();

    }

    /**
     * A Dialog window that will ask the user which question he/she wants to remove
     */
    private void removeQuestionDialogWindow() {
        List<String> choices = xmlManager.readQuestions();

        ChoiceDialog<String> dialog = new ChoiceDialog<>("Choose question", choices);
        dialog.setTitle("Remove Question");
        dialog.setHeaderText("Choose the the question to be removed from the game");
        dialog.setContentText("Choose question:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String resultString = result.get();
            if (!resultString.equals("Choose question")) {
                int index = choices.indexOf(resultString);
                xmlManager.deleteQAElement(index + 1);
            }
        }
    }

    /**
     * Helper method that manipulates the mainDisplay
     * @param text The text to be displayed on the mainDisplay
     */
    public void setMainDisplayText(String text) {
        mainDisplay.setText(text);
    }


}



package sample;

import javafx.application.Platform;

public class GameRunnable implements Runnable {


    private volatile boolean shutdown = false;

    private Controller controller;
    private Game game;
    private Boolean answer;

    /**
     * Constructor to start our runnable
     * @param controller the controler from where we control the display and store the lock
     * @param game the game with the game logic
     */
    public GameRunnable(Controller controller, Game game) {
        this.controller = controller;
        this.game = game;
    }


    /**
     * The main loop that will keep track of the game state. Here we will execute the methods from our Game object.
     * From here we will call {@link GameRunnable#askQuestion() askQuestion},
     * {@link GameRunnable#notifyOfQuestionResult() notifyOfQuestionResult()}
     * and {@link GameRunnable#notfiyOfFinalScore()  notfiyOfFinalScore()}
     * Note that all methods here will skip their executing if the shutDown flag is raised
     */
    @Override
    public void run() {
        controller.continueLock.lock();
        try {
            while (!game.isGameFinished() && !shutdown) {
                askQuestion();
                controller.continueCondition.await();
                notifyOfQuestionResult();
                Thread.sleep(3000);
            }
            notfiyOfFinalScore();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            controller.continueLock.unlock();
        }
    }

    /**
     * Will update our mainDisplay with the next question
     */
    private void askQuestion() {
        if (!shutdown) {
            String questionText = game.getPlayersTurnName() + ":\n" + game.getNextQuestions();
            Platform.runLater(() -> controller.setMainDisplayText(questionText));
        }
    }

    /**
     * Will update our mainDisplay of the result, depending on if the user answered right or wrong
     */
    private void notifyOfQuestionResult() {
        if (!shutdown) {
            if (game.isAnswerCorrect(answer)) {
                Platform.runLater(() -> controller.setMainDisplayText("Correct!"));
            } else {
                Platform.runLater(() -> controller.setMainDisplayText("Wrong!"));
            }
        }
    }

    /**
     * Will update our mainDisplay with the final score.
     * Note that we before we end this thread call {@link Controller#endGame() endGame()} to prevent unwanted userInput.
     */
    private void notfiyOfFinalScore() {
        if (!shutdown) {
            int[] finalScore = game.getScore();
            String[] finalPlayerNames = game.getPlayerNames();
            StringBuilder builder = new StringBuilder();
            builder.append("Final score!!!");
            for (int i = 0; i < finalScore.length; i++) {
                builder.append("\n");
                builder.append(finalPlayerNames[i]);
                builder.append(": ");
                builder.append(finalScore[i]);
                builder.append(" ");
                builder.append("Point(s)");
            }
            Platform.runLater(() -> controller.setMainDisplayText(builder.toString()));
            Platform.runLater(() -> controller.endGame());
        }
    }

    /**
     * Calling this method will stop all future method the be executed and this thread to terminate gracefully.
     * @param shutdown call true if you want to wish to terminate this thread
     */
    public void setShutdown(boolean shutdown) {
        this.shutdown = shutdown;
    }

    /**
     * Called from the controller aka userInput to remember the answer of a question
     * @param answer true of the user answered "yes"
     */
    public void setAnswer(boolean answer) {
        this.answer = answer;

    }

}
